package okoutils

import (
	"fmt"
	"math/rand"
	"runtime"
	"time"

	"gocv.io/x/gocv"
)

func ManageMemReader(globDat *int, memoryLevel chan int) { //To put other places in the host process to get a pointer to the amount of memory being used

	for lev := range memoryLevel {
		*globDat = lev
	}

}

func CheckMemory(memoryLevel chan int) { //Chan is to communicate back to other parts of the program how much memory there is
	for {
		var m runtime.MemStats
		runtime.ReadMemStats(&m)
		/*
			//fmt.Println(m)

			//fmt.Printf("Alloc = %v MiB", m.Alloc)

			//os.Create("./logfile.txt")
			//fdat, _ := ioutil.ReadFile("./logfile.txt")
			//frm := strconv.Itoa(int(m.Alloc))

			//finout := string(fdat) + frm + "\n"

			//ioutil.WriteFile("./logfile.txt", []byte(finout), 0711)
		*/
		memoryLevel <- int(m.Alloc)
		time.Sleep(1 * time.Second)
	}
}

func FindActiveCams(rangeLow int, rangeHigh int) ([]int, error) {
	var eRet error
	var retlist []int = []int{}

	for i := rangeLow; i < rangeHigh; i++ {
		tst, err := gocv.VideoCaptureDevice(i)
		if err == nil {
			retlist = append(retlist, i)

		}
		tst.Close()
	}

	if len(retlist) == 0 {
		eRet = fmt.Errorf("function FindActiveCams failed to actually find any active cameras.")
	}

	return retlist, eRet
}

func RandStr(high int) string {
	var runes []rune = []rune{}
	var outstr string = ""
	var runstring string = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"

	rand.Seed(time.Now().UnixNano())

	for _, run := range runstring {
		runes = append(runes, run)
	}

	for i := 0; i < high; i++ {
		ipt := rand.Intn(len(runes))
		outstr = outstr + string(runes[ipt])
	}

	return outstr
}
